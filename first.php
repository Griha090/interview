<?php

namespace App\Http\Controllers;


class first extends Controller
{


    public function index()
    {
        // 1

        $array = [
            ['id' => 1, 'date' => "12.01.2020", 'name' => "test1"],
            ['id' => 2, 'date' => "02.05.2020", 'name' => "test2" ],
            ['id' => 4, 'date' => "08.03.2020", 'name' => "test4" ],
            ['id' => 1, 'date' => "22.01.2020", 'name' => "test1" ],
            ['id' => 2, 'date' => "11.11.2020", 'name' => "test4" ],
            ['id' => 3, 'date' => "06.06.2020", 'name' => "test3" ],
        ];
        $uniqueArray = array_reduce($array, function ($carry, $item){
            if(empty($carry[$item['id']]))
                $carry[$item['id']] = $item;
            return $carry;
        });

        // 2
        $sortArray = collect($uniqueArray)->sortBy('id');

        // 3
        $search = $sortArray->where('id', '=', $id)->first();

        // 4
        $renameKey =  array_combine(
            array_column($uniqueArray, 'name'),
            array_column($uniqueArray, 'id')
        );

        //5
        $tags = Tags::count();
        Goods::select('goods.id', 'goods.name')
            ->join('goods_tags', 'goods.id', '=', 'goods_tags.goods_id')
            ->groupBy('goods.id', 'goods.name')
            ->havingRaw('COUNT(DISTINCT goods_tags.tag_id) = ?', [$tags])
            ->get();

        //6
        $get = Evaluations::all()
            ->groupBy('department_id')
            ->filter(function ($idRespondents){
                return $idRespondents->every(function ($idRespondent){
                    return $idRespondent->gender === true && $idRespondent->value > 5;
                });
            })
            ->keys();
    }
}

