<?php 

//Strategy pattern

interface KeyStorageStrategy {
    public function saveKey($key);
    public function getKey();
}

class FileKeyStorage implements KeyStorageStrategy {
    public function saveKey($key) {

    }

    public function getKey() {

    }
}

class DatabaseKeyStorage implements KeyStorageStrategy {
    public function saveKey($key) {

    }

    public function getKey() {

    }
}


class SecretKey {
    private $storageStrategy;

    public function __construct(KeyStorageStrategy $storageStrategy) {
        $this->storageStrategy = $storageStrategy;
    }

    public function saveKey($key) {
        $this->storageStrategy->saveKey($key);
    }

    public function getKey() {
        return $this->storageStrategy->getKey();
    }
}


$fileStorage = new FileKeyStorage();
$dbStorage = new DatabaseKeyStorage();

$keyManager = new SecretKey($dbStorage);

$keyManager->setKey('secret_key');
$retrievedKey = $keyManager->getKey();
