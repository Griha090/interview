<?php

interface HttpRequestService
{
 public function request(string $url, string $metod, array $options = []);
}
class XMLHttpService extends XMLHTTPRequestService implements HttpRequestService{
    public function request(string $url, string $metod, array $options)
    {

    }
}

class Http {
    private $service;

    public function __construct(HttpRequestService $service) {
        $this->service = $service;
    }

    public function get(string $url, array $options) {
        $this->service->request($url, 'GET', $options);
    }

    // тут не знаю функция post, поэтому метод post поставил
    public function post(string $url) {
        $this->service->request($url, 'POST');
    }
}
